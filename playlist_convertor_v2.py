import os
import urllib.parse
import argparse
import random

def decode_special_characters(encoded_string):
    decoded_string = urllib.parse.unquote(encoded_string, errors='replace')
    return decoded_string

def display_available_playlists(cantata_folder):
    playlists = []
    print("Playlists disponibles :")
    for i, file in enumerate(os.listdir(cantata_folder), start=1):
        if file.endswith(".m3u"):
            playlists.append(file)
            print(f"{i}. {file}")
    return playlists

def convert_cantata_playlist_to_m3u(cantata_playlist_file, output_m3u_file):
    with open(cantata_playlist_file, 'r') as input_file:
        cantata_lines = input_file.readlines()

    m3u_lines = ['#EXTM3U\n']
    seen_files = set()  # Pour suivre les fichiers déjà ajoutés

    random.shuffle(cantata_lines)

    for line in cantata_lines:
        line = line.strip()
        if not line.startswith('#'):
            file_url = line.split('?')[0]
            file_path = decode_special_characters(file_url.split('http://127.0.0.1:37775/run/media/nath/FIIO_X1/')[1])

            # Vérifier s'il y a un doublon
            if file_path in seen_files:
                print(f"Attention : Doublon détecté pour le fichier {file_path}")
            else:
                seen_files.add(file_path)
                m3u_lines.append(file_path + '\n')

    with open(output_m3u_file, 'w') as output_file:
        output_file.writelines(m3u_lines)

    print('Conversion complète. La playlist M3U a été enregistrée dans :', output_m3u_file)
    
    verify_files_existence(m3u_lines)

def verify_files_existence(m3u_lines):
    missing_files = []

    for line in m3u_lines[1:]:
        file_path = line.strip()

        if not os.path.isfile(file_path):
            missing_files.append(file_path)

    if missing_files:
        print("Avertissement : Certains fichiers dans la playlist ne sont pas présents à l'emplacement spécifié :")
        for missing_file in missing_files:
            print(f"- {missing_file}")
    else:
        print("Tous les fichiers de la playlist sont présents à l'emplacement spécifié.")

def display_help():
    print("Ce programme permet de convertir une playlist Cantata en un fichier M3U.")
    print("Utilisation:")
    print("  python playlist_converter.py -d <dossier_cantata>")
    print("  python playlist_converter.py -p <playlist_cantata>")
    print("Pour afficher l'aide :")
    print("  python playlist_converter.py -h")

parser = argparse.ArgumentParser(description='Conversion de playlist Cantata en fichier M3U')
parser.add_argument('-d', '--directory', type=str, help='Dossier contenant les playlists Cantata')
parser.add_argument('-p', '--playlist', type=str, help='Chemin complet de la playlist Cantata')
parser.add_argument('-o', '--output', type=str, help='Nom du fichier de sortie M3U')

args = parser.parse_args()

cantata_folder = args.directory if args.directory else '/home/nath/.local/share/cantata/mpd/playlists'

if args.directory and not os.path.isdir(cantata_folder):
    print("Le dossier spécifié n'existe pas.")
    exit(1)

if not os.path.isdir(cantata_folder):
    print(f"Le dossier Cantata par défaut '{cantata_folder}' n'existe pas.")
    print("Veuillez spécifier un dossier valide avec l'option -d.")
    exit(1)

if args.playlist:
    playlist_file = args.playlist
    if not os.path.isfile(playlist_file):
        print("Le fichier de playlist spécifié n'existe pas.")
        exit(1)
    output_m3u_file = args.output if args.output else 'playlist.m3u'
    convert_cantata_playlist_to_m3u(playlist_file, output_m3u_file)
    exit(0)

playlists = display_available_playlists(cantata_folder)

if not playlists:
    print("Aucune playlist disponible dans le dossier Cantata.")
    exit(0)

playlist_num = input("Sélectionnez le numéro de la playlist à convertir : ")
try:
    playlist_num = int(playlist_num)
    if 1 <= playlist_num <= len(playlists):
        playlist_file = os.path.join(cantata_folder, playlists[playlist_num - 1])
        output_m3u_file = args.output if args.output else 'playlist.m3u'
        convert_cantata_playlist_to_m3u(playlist_file, output_m3u_file)
    else:
        print("Numéro de playlist invalide. Veuillez réessayer.")
except ValueError:
    print("Numéro de playlist invalide. Veuillez entrer un nombre.")
